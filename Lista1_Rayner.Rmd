---
title: "Lista 1"
author: "Rayner Afonso Santos"
date: "21/09/2022"
output: html_document
---

```{r setup, include=FALSE, warning=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r packages, warning=FALSE, message=FALSE}
library(tidyverse)
library(dplyr)
library(caret)
library(magrittr)
library(glmnet)
library(cluster)
library(Hmisc)
library(groupdata2)
library(class)
```

## Exercício 1
Baixe no site os dados worldDevelopmentIndicators.csv, que contém os dados do PIB per capita
(X) e a expectativa de vida (Y ) de diversos países. O objetivo é criar preditores de Y com base em X. Em aula vimos como isso pode ser feito através de polinômios. Aqui, faremos isso via expansões de Fourier.

```{r}
wdi <- read_csv('worldDevelopmentIndicators.csv')
str(wdi)
summary(wdi)
```

1. Normalize a covariável de modo que x ∈ (0, 1). Para isso, faça x =
(x−xmin)/(xmax−xmin), onde xmin e xmax são os valores m´ınimos e máximos de x segundo a amostra usada.

```{r}
wdi$X <- (wdi$GDPercapita - min(wdi$GDPercapita)) / (max(wdi$GDPercapita) - min(wdi$GDPercapita))

summary(wdi$X)  
```

2. Usando o método dos mínimos quadrados e a validação cruzada do tipo leave-one-out, estime o erro quadrático médio das regressões
g(x) = βb0 + βb1 sin(2πx) + βb2 cos(2πx) + βb3 sin(2π2x) + βb4 cos(2π2x) + . . . + βb2p−1 sin(2πpx) + βb2p cos(2πpx)
para p = 1, . . . , 30.

```{r}
# Criando a base de dados completa
for(p in 1:30){
  var_name <- paste0('2pi', p, 'x')
  # criando colunas de seno
  wdi[ paste0('sin(', var_name, ')') ] = sin(2*pi*p*wdi$X)
  # criando colunas de cosseno
  wdi[ paste0('cos(', var_name, ')') ] = cos(2*pi*p*wdi$X)
}

```

```{r, warning=FALSE, message=FALSE}
ajusted = function(x){
  sum( model$finalModel$coefficients  * c(1, as.vector( t(x[-1])) ) )
}

var_name_list <- NULL
mse_p <- c()
mse_p_auto <- c()
for(p in 1:30){
  var_name_p <- c(paste0('sin(2pi', p, 'x)'), paste0('cos(2pi', p, 'x)'))
  var_name_list <- c(var_name_list, var_name_p)
  
  X = wdi[,var_name_list]
  y = wdi$LifeExpectancy
  
  # # eqm com a funcao de loo criada
  # mse_p[p] = loo_lm(X, y)
  
  # eqm com a funcao loo do caret
  model <- train(y ~ .,
                 data = cbind(X, y),
                 method = "lm",
                 trControl = trainControl(method = "LOOCV")
                 )
  wdi[,paste0("pred_p", p)] = model$pred$pred
  wdi[,paste0("ajust_p", p)] = apply( model$trainingData, 1, ajusted)
  mse_p_auto[p] = mean( (model$pred$obs - model$pred$pred)^2 )
}

```

3. Plote o gráfico do risco estimado vs p. Qual o valor de p escolhido? Denotaremos ele por pesc

```{r}
p_esc = which(mse_p_auto == min(mse_p_auto))

data.frame(mse_p_auto) %>% 
  ggplot(aes(x = 1:30, y = mse_p_auto)) +
  geom_point() +
  geom_vline(xintercept = p_esc, color = 'red') +
  annotate('text',
           x = 15,
           y = mean(mse_p_auto),
           label = paste0('p_esc = ', p_esc, ' com MSE = ', round(min(mse_p_auto),2)),
           color = 'red') +
  labs(x = 'p',
       y = 'MSE')

```

4. Plote os das curvas ajustadas para p = 1, p = pesc e p = 30 sob o gráfico de disperão de X por Y . Qual curva parece mais razoável? Use um grid de valores entre 0 e 1 para isso. Como estes ajustes se comparam com o visto em aula via polinômios? Discuta.

```{r}
fig_1 <- wdi %>% 
  ggplot(aes(x = GDPercapita, y = LifeExpectancy)) +
  geom_point() +
  geom_line(aes(y = ajust_p1), col = 'blue') +
  labs(title = 'p = 1')
  

col_pred_pesc = paste0("pred_p", p_esc)
col_ajust_pesc = paste0("ajust_p", p_esc)
fig_pesc <- wdi %>% 
  ggplot(aes( x = GDPercapita, y = LifeExpectancy)) +
  geom_point() +
  geom_line(aes( y = !!rlang::ensym(col_ajust_pesc) ), col = "blue" ) +
  labs(title = paste0('p = ', p_esc))
  
fig_30 = wdi %>% 
  ggplot(aes( x = GDPercapita, y = LifeExpectancy)) +
  geom_point() +
  geom_line(aes( y = ajust_p30), col = "blue" ) +
  labs(title = 'p = 30')

gridExtra::grid.arrange(fig_1, fig_pesc, fig_30, nrow = 1)

# Podemos perceber pelo gráfico que a curva mais plausível é a com p = 3 (p escolhido). Ao verificar a curva p = 1 notamos underfitting, pois a função de predição é muito simples para predizer o conjunto de dados, ou seja, o Viés é alto. O contrário é válido para p = 30, notamos uma curva onde a função de predição é muito complexa e a variância é muito alta causando overfitting.

```

5. Plote o gráfico de valores preditos versus ajustados para p = 1, p = pesc e p = 30 (não se esqueça de usar o leave-one-out para calcular os valores preditos! Caso contrário você terá problemas de overfitting novamente). Qual p parece ser o mais razoável?

```{r}
fig_1 = wdi %>% 
  ggplot(aes( x = pred_p1, y = ajust_p1)) +
  geom_point() +
  geom_abline(slope = 1, intercept = 0, color = "red") +
  labs(title = 'p = 1',
       x = 'Valor predito (Leave-One-Out)',
       y = 'Valor ajustado')

col_pred_pesc = paste0("pred_p", p_esc)
col_ajust_pesc = paste0("ajust_p", p_esc)
fig_pesc = wdi %>%
  ggplot(aes( x = !!rlang::ensym(col_pred_pesc), y = !!rlang::ensym(col_ajust_pesc))) +
  geom_point() +
  geom_abline(slope = 1, intercept = 0, color = "red") +
  ggtitle(paste0("p = ", p_esc)) +
  xlab("Valor predito (Leave-One-Out)") +
  ylab("Valor ajustado")

fig_30 = wdi %>% 
  ggplot(aes( x = pred_p30, y = ajust_p30)) +
  geom_point() +
  geom_abline(slope = 1, intercept = 0, color = "red") +
  ggtitle("p = 30") +
  xlab("Valor predito (Leave-One-Out)") +
  ylab("Valor ajustado")

gridExtra::grid.arrange(fig_1, fig_pesc, fig_30, nrow = 1)


```

6. Quais vantagens e desvantagens de se usar validação cruzada do tipo leave-one-out versus o data-splitting?

```{r}
# Data-splitting é uma abordagem muito interessante de ser usada quando temos um conjunto de dados grande, visto que podemos separar os dados em dois subconjuntos, um para treinamento do modelo e uma outra parte para a validação do modelo estimando a melhor função de predição. No caso de não termos um conjunto de dados grande o suficiente a melhor opção é a validação cruzada do tipo leave-one-out, no qual fará o treinamento com n-1 observações, deixando uma amostra de fora e alternando a observação que fica de fora para validar o modelo. A desvantagem da validação cruzada do tipo leave-one-out é que o custo computacional será extremamente alto,  pois teremos que ajustar o mesmo modelo n vezes.

```

7. Ajuste a regressão Lasso (Frequentista e Bayesiana) e discuta os resultados encontrados.

```{r}
# Lasso Frequentista
X_train = X
y_train = y

cv.out = cv.glmnet( X_train %>% as.matrix(),
                    y_train,
                    alpha = 1,
                    lambda = seq(0,10,length.out=1000))

lasso_results = data.frame(cv.out$lambda, cv.out$cvm, cv.out$nzero)
names(lasso_results) = c("lambda", "cvm", "nzero")

lambda_esc = lasso_results$lambda[lasso_results$cvm == min(lasso_results$cvm)]

data.frame(lasso_results) %>% 
  ggplot(aes(x = lambda, y = cvm)) + 
  geom_point( ) + 
  geom_vline(xintercept = lambda_esc,
             color = "red") +
  annotate("text",
           x = lambda_esc + 3,
           y = max(lasso_results$cvm), 
           label = paste0("lambda = ", round(lambda_esc,4), " com RMSE = ", round(min(lasso_results$cvm), 2 )),
           colour = "red") +
  xlab("lambda") +
  ylab("Mean cross-validated error")

```

## Exercício 2
Neste exercício você irá implementar algumas técnicas vistas em aula para o banco de dados das
faces. O objetivo aqui é conseguir criar uma função que consiga predizer para onde uma pessoa está olhando com base em uma foto. Iremos aplicar o KNN para esses dados, assim como uma regressão linear. Como não é possível usar o método dos mínimos quadrados quando o número de covariáveis é maior que o número de observações, para esta segunda etapa iremos usar o lasso.

(a) Leia o banco dadosFacesAltaResolucao.txt. A primeira coluna deste banco contém a variável
que indica a direção para a qual o indivíduo na imagem está olhando. As outras covariáveis contém os pixels relativos a essa imagem, que possui dimensão 64 por 64. Utilizando os comandos fornecidos, plote 5 imagens deste banco.
Divida o conjunto fornecido em treinamento (aproximadamente 60% das observa¸c˜oes), valida¸c˜ao (aproximadamente
20% das observa¸c˜oes) e teste (aproximadamente 20% das observa¸c˜oes). Utilizaremos o conjunto de treinamento
e valida¸c˜ao para ajustar os modelos. O conjunto de teste ser´a utilizado para testar sua performance.

```{r}
far <- read.table('dadosFacesAltaResolucao.txt', header = T)

matrix(far[1,-1] %>% unlist(), nrow = 64, ncol = 64, byrow = FALSE) %>% image()

matrix(far[2,-1] %>% unlist(), nrow = 64, ncol = 64, byrow = FALSE) %>% image()

matrix(far[3,-1] %>% unlist(), nrow = 64, ncol = 64, byrow = FALSE) %>% image()

matrix(far[4,-1] %>% unlist(), nrow = 64, ncol = 64, byrow = FALSE) %>% image()

matrix(far[5,-1] %>% unlist(), nrow = 64, ncol = 64, byrow = FALSE) %>% image()

```

```{r}
set.seed(123)
index <- partition(as.data.frame(far$y),
                   p = c(train = 0.6,
                         valid = 0.2, 
                         test = 0.2)
                   )
# train dataset
far_train <- far[index$train, ]
X_train <- far_train %>% select( -y )
y_train <- far_train %>% select( y )

# validation dataset
far_val <- far[index$valid, ]
X_val <- far_val %>% select( -y )
y_val<- far_val %>% select( y )

# test dataset
far_test <- far[index$test, ]
X_test <- far_test %>% select( -y )
y_test <- far_test %>% select( y )

```


(b) Qual o número de observações? Qual o número de covariáveis? O que representa cada covariável?

```{r}
# O banco de dados original tem 698 observações e 4096 covariáveis. Ao repartir em treino, validação e teste o banco de cada etapa ficou com, respectivamente, 418, 140 e 140 observações.

```

(c) Para cada observação x do conjunto de teste, calcule o estimador da função de regressão r(x) dado pelo método dos k vizinhos mais próximos com k = 5. Você pode usar as funções vistas em aula.

```{r}


```



