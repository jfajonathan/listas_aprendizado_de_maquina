rm(list = ls())
library(e1071)
library(MASS)
library(FNN)
library(Epi)
library(plyr)
### Exercicio 2)

dados <- read.delim("~/Aprendizagem de Maquinas/titanic.txt")

dados$Survived <-ifelse(dados$Survived =="Yes",1,0)

set.seed(13)

indices <- 1:dim(dados)[1]

######### Dados Treino

size_treino <- round(0.6*length(indices))

indices_treino<-  sample(indices,size=size_treino,replace = F)

dados_treino <- dados[indices_treino,]

########## Dados Teste

size_teste <- round(0.2*length(indices))

indices_pos_treino <- indices[-indices_treino]

indices_teste <-  sample(indices_pos_treino ,size= size_teste ,replace = F)

dados_teste <-  dados[indices_teste,]

######################### 2.1) Regressão Logistica

m0glm <- glm(Survived ~ ., data = dados_treino, family = "binomial")

tabglm <- summary(m0glm)$coefficients

### Ajustando esse modelo ao conjunto de treinamento, dados_treino têm-se os seguintes coeficientes estimados:

tabglm

##### Risco estimado

pglm <- predict(m0glm, newdata = dados_teste, type = "response")

pred.glm <- ifelse(pglm >= 0.5, 1, 0)

errglm <- sum(dados_teste[, 4] != pred.glm)/nrow(dados_teste)

########################## 2.2) Regressão Linear


m0lm <- lm(as.integer(Survived) ~ ., data = dados_treino)

tablm <- summary(m0lm)$coefficients

### Ajustando esse modelo ao conjunto de treinamento, dados_treino têm-se os seguintes coeficientes estimados:

tabglm

# Risco estimado

plm <- predict(m0lm, newdata = dados_teste) 
pred.lm <- ifelse(plm >= 0.5, 1, 0)
errlm<- sum(dados_teste[, 4] != pred.lm)/nrow(dados_teste)

###################### 2.3) Naive Bayes

m0naive <- naiveBayes(Survived ~., data = dados_treino)

tabnaive <- do.call(cbind, m0naive$tables)

tabnaive 

####### Risco Estimado 

pnaive <- predict(m0naive, newdata = dados_teste, type = "raw")[, 2]
pred.naive <- ifelse(pnaive >= 0.5, 1, 0)
errnaive <- sum(dados_teste[, 4] != pred.naive)/nrow(dados_teste)


################### 2.4) Analise de Descriminante Linear

m0lda <- lda(Survived ~ ., data = dados_treino)
m0lda

##-------------------------------------------
## Risco estimado
plda <- predict(m0lda, newdata = dados_teste[, -4])[["posterior"]][, 2]
pred.lda <- ifelse(plda >= 0.5, 1, 0)
errlda <- sum(dados_teste[, 4] != pred.lda)/nrow(dados_teste)



######################### 2.5) Análise Discriminante Quadrática

m0qda <- qda(Survived ~ ., data = dados_treino)
m0qda

## Risco estimado
pqda <- predict(m0qda, newdata = dados_teste[, -4])[["posterior"]][, 2]
pred.qda <- ifelse(pqda >= 0.5, 1, 0)
errqda <- sum(dados_teste[, 4] != pred.qda)/nrow(dados_teste)

############   2.6) KNN

##### Separa os dados de treino para Validacao


indices <- 1:dim(dados_treino)[1]

######### Dados Treino

size_validacao <- round(0.2*length(indices))

indices_validacao<-  sample(indices,size=size_validacao,replace = F)

dados_validacao <- dados_treino[indices_validacao,]

dados_treino_sep <- dados_treino[-indices_validacao,]


################################
dados_treino_sep$Class <- revalue(dados_treino_sep$Class,
                             c("1st"="1", "2nd"="2", "3rd"="3", "Crew"="4"))

dados_treino_sep$Class <- as.numeric(dados_treino_sep$Class)

#######################################################

dados_validacao$Class <- revalue(dados_validacao$Class,
                                  c("1st"="1", "2nd"="2", "3rd"="3", "Crew"="4"))

dados_validacao$Class <- as.numeric(dados_validacao$Class)

###################################################
dados_treino_sep$Sex <- ifelse(dados_treino_sep$Sex =="Male",1,0)

dados_validacao$Sex   <- ifelse(dados_validacao$Sex =="Male",1,0)
###############################################

dados_treino_sep$Age <-ifelse(dados_treino_sep$Age =="Adult",1,0)

dados_validacao$Age <-ifelse(dados_validacao$Age =="Adult",1,0)

################### KNN

kseq <- seq(1, nrow(dados_treino_sep), by = 1)
errs <- sapply(kseq, function(k) {
  m0 <- FNN::knn(train = dados_treino_sep[, -4], test = dados_validacao[, -4],
                 cl = dados_treino_sep[, 4], k = k, algorithm = "kd_tree")
  sum(dados_validacao[, 4] != m0)/nrow(dados_validacao)
})
errs

plot(kseq,errs,type="l")






############# Risco Estimado

################################
dados_treino$Class <- revalue(dados_treino$Class,
                                  c("1st"="1", "2nd"="2", "3rd"="3", "Crew"="4"))

dados_treino$Class <- as.numeric(dados_treino$Class)

#######################################################

dados_teste$Class <- revalue(dados_teste$Class,
                                 c("1st"="1", "2nd"="2", "3rd"="3", "Crew"="4"))

dados_teste$Class <- as.numeric(dados_teste$Class)

###################################################
dados_treino$Sex <- ifelse(dados_treino$Sex =="Male",1,0)

dados_teste$Sex   <- ifelse(dados_teste$Sex =="Male",1,0)
###############################################

dados_treino$Age <-ifelse(dados_treino$Age =="Adult",1,0)

dados_teste$Age <-ifelse(dados_teste$Age =="Adult",1,0)







m0knn <- class::knn(train = dados_treino[, -4], test = dados_teste[, -4],
                    cl = dados_treino[, 4], k = which.min(errs), prob = TRUE)

##-------------------------------------------
## Risco estimado
pknn <- ifelse(m0knn == "Yes", attr(m0knn, "prob"),
               1 - attr(m0knn, "prob"))
pred.knn <- ifelse(pknn >= 0.5, 1, 0)
errknn <- sum(dados_teste[, 4] != pred.knn)/nrow(dados_teste)



lista <- list("GLM" = pglm, "LM" = plm, "NB" = pnaive,
              "LDA" = plda, "QDA" = pqda, "KNN" = pknn)

cbind(errglm,errlm,errnaive,errlda,errqda,errknn)

## Os modelos Regressão Logistica, Naice e An´alise de Descrimiante Quadrática tiveram o menor risco estimado.