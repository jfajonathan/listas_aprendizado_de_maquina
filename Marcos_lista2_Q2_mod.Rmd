---
title: "Untitled"
author: "Maura Vilela"
date: '2022-09-29'
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

\

Pacotes utilizados
```{r}
require(tidyverse)
```
\
Exercício 2. Baixe o conjunto de dados titanic.txt. Cada observaçâo deste banco
é relativa a um passageiro do Titanic. As covariáveis indicam características deste passageiros; a variável resposta indica se o passageiro sobreviveu ou não ao naufrágio.
Seu objetivo é criar classificadores para predizer a variável resposta com base nas covariáveis disponíveis. Para tanto, você deverá implementar os seguintes classificadores, assim como estimar seus riscos via conjunto de teste:

Base de dados
```{r}
titanic = read.csv(file.choose(), header = TRUE , sep = ";", dec = ".")
glimpse(titanic) # visualizar as variáveis
bd_titanic = titanic %>%
  transmute( n = n,
             classe = factor(Class),
             sexo = factor(Sex),
             idade = factor(Age),
             sobrevivente = factor(Survived))
glimpse(bd_titanic)
head(bd_titanic, n= 3)
fix(titanic)

```

\

Divisao do banco de dados
```{r}

qte_sorteio = 2201*0.7 # definir a quantidade de numeros para o sorteio

set.seed(1) #semente para realizar sorteio
bd_treinamento = titanic %>%
  mutate(sorteio = sample(nrow(titanic), 2201),
         treino = factor(sorteio <= 1541))
head(bd_treinamento, n= 3)

```


\


1 Regressão logistica mostre os coeficientes estimados

```{r}

modelo_treinamento = (filter(bd_treinamento, treino == "TRUE")) %>%
  transmute(Classe = factor(Class),
            Sexo = factor(Sex),
            Idade = factor(Age),
            Sobrevivente = factor(Survived))
names(modelo_treinamento)


# Modelo de regressão logistica

require(glmnet)

regressão.1 = glmnet(Sobrevivente ~ Classe + Sexo + Idade, alpha = 1, family = "binomial",
                     date = modelo_treinamento)

# logistica no modeSurvivedlo glm

regressão.glm= glm(Sobrevivente ~ Classe + Sexo + Idade , binomial(link = logit), data = modelo_treinamento)
summary(regressão.glm)
  
```

\


